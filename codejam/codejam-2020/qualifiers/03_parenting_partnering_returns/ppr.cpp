#include <algorithm>
#include <iostream>
#include <vector>


struct event
{
    int start;
    int end;
    int original_index;
    char attended_by;
};

using timetable = std::vector<event>;



timetable get_timetable()
{
    int n;
    std::cin >> n;

    timetable t(n);
    for (int i = 0; i < n; ++i)
    {
        event e;
        std::cin >> e.start;
        std::cin >> e.end;
        e.original_index = i;

        t.at(i) = e;
    }

    std::sort(begin(t), end(t),
        [](const event& e1, const event& e2)
        {
            return (e1.start == e2.start) ?
                (e1.end < e2.end) :
                (e1.start < e2.start);
        }
    );

    return t;
}


std::vector<timetable> get_cases()
{
    int T;
    std::cin >> T;

    std::vector<timetable> cases(T);
    for (int i = 0; i < T; ++i) { cases.at(i) = get_timetable(); }

    return cases;
}


timetable assign_attendees(timetable t)
{
    int c_end = 0;
    int j_end = 0;

    for (event& e : t)
    {
        if (e.start >= c_end)
        {
            e.attended_by = 'C';
            c_end = e.end;
        }
        else if (e.start >= j_end)
        {
            e.attended_by = 'J';
            j_end = e.end;
        }
        else { return {}; }
    }

    return t;
}


std::string determine_order(const timetable& t)
{
    timetable u (assign_attendees(t));

    if (u.size() == 0) { return "IMPOSSIBLE"; }

    std::sort(begin(u), end(u),
        [](const event& e1, const event& e2) { return e1.original_index < e2.original_index; }
    );

    std::string schedule = "";
    for (const event& e : u) { schedule.push_back(e.attended_by); }

    return schedule;
}


void print_case(const int x, const std::string& y)
{
    std::cout << "Case #" << (x + 1) << ": " << y << "\n";
}


int main()
{
    const std::vector<timetable> cases = get_cases();

    for (std::size_t i = 0; i < cases.size(); ++i)
    {
        print_case(i, determine_order(cases.at(i)));
    }

    return 0;
}
