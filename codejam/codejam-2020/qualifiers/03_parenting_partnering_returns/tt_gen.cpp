#include <algorithm>
#include <iostream>
#include <random>
#include <vector>



struct event
{
    int start;
    int end;
};

using timetable = std::vector<event>;


timetable generate_timetable()
{
    constexpr int end_time = 24 * 60;

    std::random_device rd;
    std::mt19937 gen(rd());
    
    timetable t;
    
    std::uniform_int_distribution<> dist(0, end_time / 8);
    int event_end = dist(gen);

    while (event_end < end_time)
    {
        dist = std::uniform_int_distribution<>(event_end, end_time);
        const int start_time = dist(gen);
        dist = std::uniform_int_distribution<>(start_time, end_time);

        event e;
        
        e.start = start_time;
        e.end   = dist(gen);
        
        event_end = e.end;

        t.push_back(e);
    }

    return t;
}


event generate_impossible_event(const timetable& t1, const timetable& t2)
{
    std::random_device rd;
    std::mt19937 gen(rd());

    event e1;
    event e2;
    
    std::uniform_int_distribution<> dist(0, t1.size() - 1);
    e1 = t1.at(dist(gen));

    dist = std::uniform_int_distribution<>(0, t2.size() - 1);
    e2 = t2.at(dist(gen));

    event e;
    e.start = (e1.start < e2.start) ? e1.start : e2.start;
    e.end = (e1.end > e2.end) ? e1.end : e2.end;

    return e;
}


timetable create_random_case(const timetable& t1, const timetable& t2)
{
    timetable t(t1.size() + t2.size());

    std::merge(begin(t1), end(t1),
        begin(t2), end(t2),
        begin(t),
        [](const event& e1, const event& e2)
        {
            return (e1.start == e2.start) ?
                (e1.end < e2.end) :
                (e1.start < e2.start);
        }
    );

    // t.push_back(generate_impossible_event(t1, t2));

    // std::random_device rd;
    // std::mt19937 gen(rd());

    // std::uniform_int_distribution<> dist(0, (24 * 60) - 1);
    // const int start_time = dist(gen);
    // dist = std::uniform_int_distribution<>(start_time + 1, 24 * 60);
    // const int end_time = dist(gen);

    // event e1 { start_time, end_time };
    // event e2 { start_time, end_time };
    // event e3 { start_time, end_time };

    // t.push_back(e1);
    // t.push_back(e2);
    // t.push_back(e3);

    std::random_shuffle(begin(t), end(t));

    return t;
}


int main()
{
    constexpr int T = 1000;

    std::cout << T << "\n";

    for (int i = 0; i < T; ++i)
    {
        timetable t = create_random_case(generate_timetable(), generate_timetable());

        std::cout << t.size() << std::endl;

        for (const event& e : t)
        {
            std::cout << e.start << " " << e.end << "\n";
        }
    }

    return 0;
}