#include <iostream>
#include <vector>



bool gen_trace(const int index, const int n, const int k, std::vector<int>& trace)
{
    for (int i = trace.at(index); i <= n; ++i)
    {
        trace.at(index) = i;

        if (index + 1 < n)
        {
            trace.at(index + 1) = 1;
            if (gen_trace(index + 1, n, k, trace)) return true;
        }
        else
        {
            int sum = 0;
            for (const int m : trace) { sum += m; }

            if (sum == k) { return true; }
        }
    }

    return false;
}


bool find_trace(const int n, const int k, std::vector<int>& trace)
{
    return gen_trace(0, n, k, trace);
}


std::string get_result()
{
    int N, K;
    std::cin >> N >> K;

    std::vector<int> trace(N, 1);

    bool found = false;
    
    while (!found)
    {
        found = find_trace(N, K, trace);
        
        for (const int i : trace)
        {
            std::cout << i << " ";
        }

        std::cout << std::endl;
    }

    return "";
}


int main()
{
    int T;
    std::cin >> T;

    get_result();

    for (int i = 0; i < T; ++i)
    {

    }

    return 0;
}