#include <iostream>
#include <unordered_set>
#include <vector>



using matrix = std::vector<std::vector<int>>;


matrix get_matrix(const int size)
{
    matrix m = matrix(size, std::vector<int>(size, 1));
    
    for (int i = 0; i < size; ++i)
    {
        for (int j = 0; j < size; ++j)
        {
            int n;
            std::cin >> n;

            m.at(i).at(j) = n;
        }
    }

    return m;
}


std::vector<matrix> get_matrices()
{
    int T;
    std::cin >> T;

    std::vector<matrix> matrices;

    for (int t = 0; t < T; ++t)
    {
        int size;
        std::cin >> size;

        matrix m = get_matrix(size);

        matrices.push_back(m);
    }

    return matrices;
}


int compute_trace(const matrix& m)
{
    const int n = m.size();

    int trace = 0;
    for (int i = 0; i < n; ++i) { trace += m.at(i).at(i); }

    return trace;
}


bool repeats(const std::vector<int>& v)
{
    std::unordered_set<int> s;

    for (const int i : v)
    {
        auto res = s.emplace(i);
        if (!res.second) { return true; }
    }

    return false;
}


int count_row_repeats(const matrix& m)
{
    int count = 0;
    for (const std::vector<int>& row : m) { count += repeats(row) ? 1 : 0; }
    return count;
}


int count_col_repeats(const matrix& m)
{
    int count = 0;
    for (std::size_t i = 0; i < m.size(); ++i)
    {
        std::vector<int> col;
        for (std::size_t j = 0; j < m.size(); ++j) { col.push_back(m.at(j).at(i)); }
        count += repeats(col) ? 1 : 0;
    }

    return count;
}


void print_matrix(const matrix& m)
{
    for (const std::vector<int>& row : m)
    {
        for (const int i : row)
        {
            std::cout << i << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}


void print_case(const int x, const int k, const int r, const int c)
{
    std::cout << "Case #" << (x + 1) << ": " << k << " " << r << " " << c << "\n";
}


int main()
{
    const std::vector<matrix> matrices = get_matrices();

    for (std::size_t x = 0; x < matrices.size(); ++x)
    {
        const matrix& m = matrices.at(x);
        print_case(x,
            compute_trace(m),
            count_row_repeats(m),
            count_col_repeats(m)
        );
    }

    return 0;
}
