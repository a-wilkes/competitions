#include <bitset>
#include <iostream>
#include <string>
#include <unordered_map>



std::string request_subsequence(const int start, const int length)
{
    std::string s(length, '2');

    for (int i = 0; i < length; ++i)
    {
        std::cout << start + i + 1 << std::endl;

        char bit;
        std::cin >> bit;

        s.at(i) = bit;
    }

    return s;
}


std::string request_alternates()
{
    std::string s(10, '2');

    for (int i = 0; i < 20; i += 2)
    {
        std::cout << i + 1 << std::endl;

        char bit;
        std::cin >> bit;

        s.at(i / 2) = bit;
    }

    return s;
}


std::string flip(const std::string& s)
{
    std::string s_prime(s.length(), '2');

    for (std::size_t i = 0; i < s.length(); ++i)
    {
        const char c = (s.at(i) == '0') ? '1' : '0';
        s_prime.at(i) = c;
    }

    return s_prime;
}


bool matches_part(const std::string& lower, const std::string& upper)
{
    for (int i = 0; i < 5; ++i)
    {
        if (lower.at(5 + i) != upper.at(i)) { return false; }
    }

    return true;
}


bool overlaps(const std::string& lower, const std::string& upper)
{
    return (matches_part(lower, upper) || matches_part(lower, flip(upper)));
}


std::string reverse(const std::string& s)
{
    std::string s_prime(s.length(), '2');
    for (std::size_t i = 0; i < s.length(); ++i) { s_prime.at(s.length() - 1 - i) = s.at(i); }
    return s_prime;
}



std::string determine_array(const int n)
{
    constexpr int start = 0;
    constexpr int overlap = 5;
    constexpr int middle = 10;

    if (n == 10) { return request_subsequence(start, 10); }

    auto b1 = request_subsequence(start, 10);
    auto b2 = request_subsequence(overlap, 10);

    if (overlaps(b1, b2))
    {
        // b1 is before b3 and b1 may need flipping
        if (!matches_part(b1, b2)) { b1 = flip(b1); }
        // b1 is now correct

        std::string b3 = request_subsequence(middle, 5);

        if (overlaps(b2, b3))
        {
            if (!matches_part(b2, b3))
            {
                b1 = flip(b1);
                b2 = flip(b2);
            }

            auto b4 = request_subsequence(middle + 5, 5);

            return b1 + b3 + b4;
        }
        else
        {
            b1 = reverse(b1);
            b2 = reverse(b2);

            if (!matches_part(b2, b3))
            {
                b1 = flip(b1);
                b2 = flip(b2);
            }

            auto b4 = request_subsequence(start, 5);

            return b4 + b2.substr(0, 5) + b1;
        }
        

        return b1 + b2;
    }
    else
    {
        // b1 is after b2
        b1 = reverse(b1);
        if (!matches_part(b2, b1)) { b1 = flip(b1); }
        // b1 is now correct

        std::string b3 = request_subsequence(middle, 5);

        if (overlaps(b2, b3))
        {
            if (!matches_part(b2, b3))
            {
                b1 = flip(b1);
                b2 = flip(b2);
            }

            auto b4 = request_subsequence(start, 5);

            return b4 + b2.substr(0, 5) + b1;
        }
        else
        {
            b1 = reverse(b1);
            b2 = reverse(b2);

            if (!matches_part(b2, b3))
            {
                b1 = flip(b1);
                b2 = flip(b2);
            }

            auto b4 = request_subsequence(middle + 5, 5);

            return b1 + b3 + b4;
        }
    }
}


int main()
{
    int T, B;

    std::cin >> T;
    std::cin >> B;

    for (int t = 0; t < T; ++t)
    {
        std::unordered_map<std::string, int> options;
        
        for (int i = 0; i < 4; ++i)
        {
            const auto option = determine_array(B);

            auto res = options.emplace(option, 1);
            if (!res.second) { options.at(option) += 1; }
        }

        std::pair<std::string, int> best_pair = *begin(options);
        for (const auto& o : options)
        {
            if (o.second >= best_pair.second) { best_pair = o; }
        }

        std::string best = best_pair.first;
        const auto matcher = request_subsequence(0, 10);

        bool done = true;

        for (std::size_t i = 0; i < matcher.length(); ++i)
        {
            if (matcher.at(i) != best.at(i)) { done = false; }
        }

        if (done)
        {
            std::cout << best << std::endl;
        }
        else
        {
            done = true;

            best = flip(best);
            for (std::size_t i = 0; i < matcher.length(); ++i)
            {
                if (matcher.at(i) != best.at(i)) { done = false; }
            }

            if (done)
            {
                std::cout << best << std::endl;
            }
            else
            {
                done = true;

                best = reverse(best);
                for (std::size_t i = 0; i < matcher.length(); ++i)
                {
                    if (matcher.at(i) != best.at(i)) { done = false; }
                }
                
                if (done)
                {
                    std::cout << best << std::endl;
                }
                else
                {
                    best = flip(best);
                    std::cout << best << std::endl;
                }   
            }
        }

        char resp;
        std::cin >> resp;

        if (resp == 'N') { return -1; }
    }

    return 0;
}
