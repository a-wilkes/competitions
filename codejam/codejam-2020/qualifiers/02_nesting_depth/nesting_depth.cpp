#include <iostream>
#include <string>
#include <vector>



std::vector<std::string> get_strings()
{
    int T;
    std::cin >> T;

    std::vector<std::string> inputs(T, "");
    for (int i = 0; i < T; ++i)
    {
        std::string s;
        std::cin >> s;

        inputs.at(i) = s;
    }

    return inputs;
}


std::string build_string(const std::string& s)
{
    std::string s_prime = "";
    int depth = 0;

    for (const char c : s)
    {
        const int n = c - '0';

        if (n > depth) { s_prime += std::string(n - depth, '(') + c; }
        else { s_prime += std::string(depth - n, ')') + c; }

        depth = n;
    }

    return s_prime + std::string(depth, ')');
}


void print_case(const int x, const std::string& y)
{
    std::cout << "Case #" << (x + 1) << ": " << y << "\n";
}


int main()
{
    std::vector<std::string> inputs = get_strings();

    for (std::size_t i = 0; i < inputs.size(); ++i)
    {
        print_case(i, build_string(inputs.at(i)));
    }

    return 0;
}
