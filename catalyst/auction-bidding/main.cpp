#include <iostream>
#include <vector>
#include <unordered_map>
#include <string>
#include <sstream>


std::vector<std::string> split(const std::string& s)
{
    std::vector<std::string> tokens;
    std::string token;
    std::stringstream ss (s);

    while (std::getline(ss, token, ',')) { tokens.push_back(token); }

    return tokens;
}

int main()
{
    std::string input;
    std::cin >> input;
    std::vector<std::string> tokens = split(input);

    std::vector<std::string> output;

    int current_price = std::stoi(tokens.at(0));
    const int buy_now_price = std::stoi(tokens.at(1));
    output.push_back("-");
    output.push_back(std::to_string(current_price));
    
    std::string last_bidder = tokens.at(2);
    int last_bid = std::stoi(tokens.at(3));
    output.push_back(last_bidder);
    output.push_back(std::to_string(current_price));

    for (unsigned int i = 4; i < tokens.size(); i += 2)
    {
        std::string next_bidder = tokens.at(i);
        int next_bid = std::stoi(tokens.at(i + 1));

        if (next_bid >= buy_now_price && buy_now_price != 0)
        {
            std::cout << "bought" << std::endl;
            next_bid = buy_now_price;
        }

        if (next_bid > last_bid)
        {
            if (next_bidder != last_bidder)
            {
                current_price = last_bid + 1;
                last_bidder = next_bidder;
                last_bid = next_bid;

                output.push_back(last_bidder);
                output.push_back(std::to_string(current_price));
            }
            else
            {
                last_bid = next_bid;
            }
            
        }
        else if (next_bid < last_bid)
        {
            current_price = next_bid + 1;
            output.push_back(last_bidder);
            output.push_back(std::to_string(current_price));
        }
        else
        {
            current_price = last_bid;
            output.push_back(last_bidder);
            output.push_back(std::to_string(current_price));
        }
        

        std::cout << "price: " << current_price << std::endl;
        std::cout << "last: " << last_bidder << ", " << last_bid << std::endl << std::endl;


    }

    std::cout << "winner: " << last_bidder << "," << current_price << std::endl;
    output.push_back(last_bidder);
    output.push_back(std::to_string(current_price));

    for (const auto& s : output) { std::cout << s << ","; }
    std::cout << std::endl;

    return 0;
}