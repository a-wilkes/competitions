#include "util.h"
#include <iostream>
#include <sstream>

std::vector<int> split_ints(const std::string& s, const char delimiter)
{
    std::vector<int> tokens;
    std::string token;
    std::stringstream ss (s);

    while (std::getline(ss, token, delimiter)) { tokens.push_back(std::stoi(token)); }

    return tokens;
}


int main()
{
    std::string input;
    std::cin >> input;

    std::stringstream ss (input);
    std::string token;
    std::getline(ss, token, ':');

    const int rounds = std::stoi(token);
    std::vector<int> throws = split_ints(input.substr(token.size() + 1, input.size()), ',');

    log(rounds);
    log(throws);

    std::vector<int> scores;

    int strikes = 0;
    for (int i = 0; i < rounds; ++i)
    {
        const int index = i * 2 - strikes;

        const int throw_one = throws.at(index);
        if (throw_one == 10)
        {
            scores.push_back(throw_one);
            strikes += 1;
        }
        else { scores.push_back(throw_one + throws.at(index + 1)); }

        log(std::string("score: ").append(std::to_string(scores.at(i))));

        if (scores.at(i) == 10)
        {
            log(std::string("index: ").append(std::to_string(index)));
            log(std::string("strikes: ").append(std::to_string(strikes)));
            if (throw_one == 10)
            {
                const int i1 = index + 1;
                const int i2 = index + 2;

                scores.at(i) += throws.at(i1);
                log(i1);
                
                scores.at(i) += throws.at(i2);
                log(i2);
            }
            else
            {
                scores.at(i) += throws.at(index + 2);
                log(index + 2);
            }
            
        }

        if (i != 0) { scores.at(i) += scores.at(i - 1); }
    }

    log(scores);

    return 0;
}