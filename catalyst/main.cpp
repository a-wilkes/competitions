#include <iostream>
#include <vector>

void log(const std::vector<int>& v)
{
    if (v.size() == 0) { return; }
    for (const int i : v) { std::cout << i << " "; }
    std::cout << std::endl;
}

void log(const std::vector<int>& v, const std::string& delim)
{
    if (v.size() == 0) { return; }
    for (const int i : v) { std::cout << i << delim; }
    std::cout << std::endl;
}

std::vector<int> get_serpentine(const int n_rows, const int n_cols)
{
    std::vector<int> out;

    for (int i = 0; i < n_rows * n_cols; ++i)
    {
        const int i_row = i / n_cols;
        int i_col;

        if (i_row % 2 == 0) { i_col = i % n_cols; }
        else { i_col = n_cols - (i % n_cols) - 1; }

        const int plot = (i_row * n_cols) + i_col + 1;
        out.push_back(plot);
    }

    return out;
}

std::vector<std::vector<int>> generate_field(const int n_rows, const int n_cols)
{
    std::vector<std::vector<int>> field;

    for (int i = 0; i < n_rows; ++i)
    {
        std::vector<int> row;
        for (int j = 0; j < n_cols; ++j) { row.push_back((i * n_cols) + j + 1); }
        field.push_back(row);
    }

    return field;
}

std::vector<int> get_serpentine(const int n_rows, const int n_cols, const int s_row, const int s_col)
{
    std::vector<std::vector<int>> field = generate_field(n_rows, n_cols);
    std::vector<int> out;

    int x = s_col;
    int y = s_row;

    bool down  = s_row != n_rows;
    bool right = s_col != n_cols;

    while (out.size() != static_cast<size_t>(n_rows * n_cols))
    {
        out.push_back(field.at(y - 1).at(x - 1));
        x += (right) ? 1 : -1;

        if ((right && x == n_cols + 1) || (!right && x == 0))
        {
            right = !right;
            x += (right) ? 1 : -1;
            y += (down) ? 1 : -1;
        }
    }

    return out;
}

std::vector<int> get_serpentine(const int n_rows, const int n_cols, const int s_row, const int s_col, const char dir)
{
    std::vector<std::vector<int>> field = generate_field(n_rows, n_cols);
    std::vector<int> out;

    int x = s_col;
    int y = s_row;

    bool down  = s_row != n_rows;
    bool right = s_col != n_cols;

    if (dir == 'O' || dir == 'W')
    {
        while (out.size() != static_cast<size_t>(n_rows * n_cols))
        {
            out.push_back(field.at(y - 1).at(x - 1));
            x += (right) ? 1 : -1;

            if ((right && x == n_cols + 1) || (!right && x == 0))
            {
                right = !right;
                x += (right) ? 1 : -1;
                y += (down) ? 1 : -1;
            }
        }
    }
    else
    {
        while (out.size() != static_cast<size_t>(n_rows * n_cols))
        {
            out.push_back(field.at(y - 1).at(x - 1));
            y += (down) ? 1 : -1;

            if ((down && y == n_rows + 1) || (!down && y == 0))
            {
                down = !down;
                y += (down) ? 1 : -1;
                x += (right) ? 1 : -1;
            }
        }
    }
    
    return out;
}

std::vector<int> get_circular(const int n_rows, const int n_cols, const int s_row, const int s_col, const char dir)
{
    std::vector<std::vector<int>> field = generate_field(n_rows, n_cols);
    std::vector<int> out;

    int x = s_col;
    int y = s_row;

    bool down  = dir == 'S';
    bool right = s_col != n_cols;

    int index = 0;
    while (out.size() != static_cast<size_t>(n_rows * n_cols))
    {
        out.push_back(field.at(y - 1).at(x - 1));
        y += (down) ? 1 : -1;

        if ((down && y == n_rows + 1) || (!down && y == 0))
        {
            down = !down;
            y += (down) ? 1 : -1;

            index += 1;
            if (right) { x = (index % 2 == 0) ? (index / 2) + 1 : (n_cols - (index / 2)); }
            else { x = (index % 2 == 0) ? (n_cols - (index / 2)) : (index / 2) + 1; }
        }
    }
    
    return out;
}

int main()
{
    std::cout << "ready\n";

    int n_rows, n_cols, s_row, s_col, width;
    char dir, modus;

    std::cin >> n_rows >> n_cols >> s_row >> s_col >> dir >> modus >> width;
    std::cout << "\n";

    if (modus == 'Z') { log(get_circular(n_rows, n_cols, s_row, s_col, dir)); }
    else { log(get_serpentine(n_rows, n_cols, s_row, s_col, dir)); }

    return 0;
}
