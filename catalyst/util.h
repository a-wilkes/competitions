#ifndef UTIL_H
#define UTIL_h

#include <functional>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>


template<typename T, typename U>
std::vector<T> split(const std::string& s, const char delimiter, std::function<T(U)> f)
{
    std::vector<T> tokens;
    std::string token;
    std::stringstream ss (s);

    while (std::getline(ss, token, delimiter)) { tokens.push_back(f(token)); }

    return tokens;
}

template<typename T, typename U>
void log(const T& s, const U& end) { std::cout << s << end; }

template<typename T>
void log(const T& s) { log(s, "\n"); }

template<typename T>
void log(const std::vector<T>& v)
{
    for (const auto& e : v) { log(e, ","); }
    log("");
}

#endif // UTIL_H
