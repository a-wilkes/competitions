#include "util.h"


int main()
{
    std::cout << "enter: ";
    int n;
    std::cin >> n;

    int x0 = 0;
    int x1 = 1;

    while (n > 0)
    {
        int temp = x0 + x1;
        x0 = x1;
        x1 = temp;

        n--;
    }

    std::cout << x0 << std::endl;

    return 0;
}