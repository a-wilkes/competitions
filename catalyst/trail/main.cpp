#include "util.h"

#include <algorithm>

std::string get_trail(int n)
{
    std::stringstream ss;

    while (n-- > 0)
    {
        std::string s;
        int i;

        std::cin >> s >> i;

        while (i-- > 0) { ss << s; }
    }

    return ss.str();
}


int get_distance(const std::string& path)
{
    int distance = 0;
    for (size_t i = 0; i < path.size(); ++i)
    {
        if (path.at(i) == 'F') { distance += 1; }
    }
    return distance;
}

int get_rectangular_area(const std::string& path)
{
    int direction = 0;
    int max_x = 0;
    int max_y = 0;
    int min_x = 0;
    int min_y = 0;

    int x = 0;
    int y = 0;

    for (size_t i = 0; i < path.size(); ++i)
    {
        const char c = path.at(i);

        if (c == 'F')
        {
            if (direction == 0)      { y += 1; }
            else if (direction == 1) { x += 1; }
            else if (direction == 2) { y -= 1; }
            else if (direction == 3) { x -= 1; }

            max_x = std::max(max_x, x);
            max_y = std::max(max_y, y);

            min_x = std::min(min_x, x);
            min_y = std::min(min_y, y);
        }
        else if (c == 'R') { direction = (direction + 1) % 4; }
        else if (c == 'L') { direction = (((direction - 1) % 4) + 4) % 4; }
    }

    return (max_x + std::abs(min_x)) * (max_y + std::abs(min_y));
}

struct Point { int x; int y; };

std::vector<Point> get_corners(const std::string& path)
{
    std::vector<Point> corners;
    Point pos = { 0, 0 };
    corners.push_back(pos);

    int direction = 0;

    for (size_t i = 0; i < path.size(); ++i)
    {
        const char c = path.at(i);

        if (c == 'F')
        {
            if (direction == 0)      { pos.y += 1; }
            else if (direction == 1) { pos.x += 1; }
            else if (direction == 2) { pos.y -= 1; }
            else if (direction == 3) { pos.x -= 1; }
        }
        else
        {
            corners.push_back(pos);

            if (c == 'R') { direction = (direction + 1) % 4; }
            else if (c == 'L') { direction = (((direction - 1) % 4) + 4) % 4; }
        }
        
    }

    return corners;
}

// shoelace formula: https://en.wikipedia.org/wiki/Shoelace_formula
double get_actual_area(const std::string& path)
{
    std::vector<Point> corners = get_corners(path);

    int area = 0;

    for (size_t i = 0; i < corners.size(); ++i)
    {
        const int j = (i + 1) % corners.size();

        const Point pos1 = corners.at(i);
        const Point pos2 = corners.at(j);

        area += pos1.x * pos2.y;
        area -= pos1.y * pos2.x;
    }

    return 0.5 * std::abs(area);
}

int count_pockets(const std::string& path)
{
    const auto corners = get_corners(path);
    log("");
    const int area_diff = get_rectangular_area(path) - get_actual_area(path);
    log(area_diff);

    std::vector<std::vector<Point>> pockets;

    std::vector<Point> current_pocket;

    bool prior = true;
    for (const auto& p : corners)
    {
        bool has_north = false;
        bool has_south = false;
        bool has_east = false;
        bool has_west = false;

        for (const auto& q : corners)
        {
            if (q.y > p.y) { has_north = true; }
            if (q.y < p.y) { has_south = true; }
            if (q.x > p.x) { has_east = true; }
            if (q.x < p.x) { has_west = true; }
        }

        if ((has_north && has_south) || (has_east && has_west))
        {
            current_pocket.push_back(p);

            if (!prior)
            {
                pockets.push_back(current_pocket);
                current_pocket.clear();
            }
            
            prior = true;
        }
        else { prior = false; }
    }

    for (const auto& v : pockets) { for (const auto& p : v) { std::cout << p.x << ", " << p.y << std::endl; } std::cout << std::endl; }

    return pockets.size(); 
}

int main()
{
    std::cout << "enter: ";
    int n;
    std::cin >> n;

    const std::string path = get_trail(n);

    std::cout << get_distance(path) << " " << get_rectangular_area(path) << " " << get_actual_area(path) << " " << count_pockets(path) << std::endl;

    return 0;
}
